package edu.ifsp.lojinha.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import edu.ifsp.lojinha.modelo.Produto;
import edu.ifsp.lojinha.persistencia.CarrinhoDAO;
import edu.ifsp.lojinha.persistencia.ProdutoDAO;



@WebServlet("/carrinho.sec")
public class CarrinhoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ProdutoDAO produtoDao;


	@Override
	public void init() throws ServletException {
		produtoDao = new ProdutoDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = (HttpSession)request.getSession();
		List<Produto> carrinho = (List<Produto>)session.getAttribute("carrinho");
		
		if (carrinho == null) {
			carrinho = new ArrayList<Produto>();
			session.setAttribute("carrinho", carrinho);
			
		} /*else if ("esvaziar".equals(cmd)) {
			carrinho = new ArrayList<Produto>();
			session.setAttribute("carrinho", carrinho);	*/
		
		
		String cmd = request.getParameter("cmd");
				
		
		
		if ("remove".equals(cmd)) {
				String paramProduto = request.getParameter("carrinho");
				int id = Integer.parseInt(paramProduto);
				if(carrinho != null) {
				carrinho.removeIf(p-> p.getId()== id);	
				}
		}

		/* Havia um comando válido na URL? */
		if (cmd != null) {
			response.sendRedirect("carrinho.sec");
		} else {		
			RequestDispatcher rd = request.getRequestDispatcher("carrinho.jsp");
			rd.forward(request, response);
		}
	}

	}

