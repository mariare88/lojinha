<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cadastro de Clientes</title>

<script>
function validar() {
	let nome = document.querySelector('#nome').value;
	let valid = nome.trim().length > 0;
	
	if (!valid) {
		Array.from(document.querySelectorAll('.error-msg')).forEach(e => {
			e.style.display = 'inline';
		});	
	}
	
	return valid;
}
</script>

<style>
.error-msg {
	display: none;
	font-size: 70%;
	color: red;
}
</style>
</head>
<body>

<h1>Cadastro de Clientes</h1>

<c:if test="${not empty requestScope.cliente}">
<p>Dados salvos!</p>
</c:if>
<form action="cadastrar" method="post" onsubmit="return validar();">
	<label for="nome">Nome: </label>
	<input type="text" id="nome" name="nome" width="40" required maxlength="40" value="${requestScope.cliente.nome}">
	<span class="error-msg">Informe um nome válido.</span>
	
	<br>
	<label>E-mail: </label> 
	<input type="email" id="email" name="email" value="${requestScope.cliente.email}">
	
	<br>
	<button type="submit">Salvar</button>
</form>

</body>
</html>