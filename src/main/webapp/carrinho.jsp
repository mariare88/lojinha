<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Lista de Clientes</title>
<link rel="stylesheet" href="css/lojinha.css">

<body>
<h1>Seus produtos</h1>

<c:choose>
	<c:when test="${empty sessionScope.carrinho}">
		<p>Seu carrinho</p>
	</c:when>
	<c:otherwise>
		<p>${sessionScope.carrinho.size()} itens no <a href="#">carrinho</a> 
		<small>(<a href="carrinho.sec?cmd=esvaziar" class="action">Esvaziar carrinho</a>)</small>
		</p>	
	</c:otherwise>
</c:choose>

<table border="1">
<tr>
	<th>Cód.</th>
	<th>Descrição</th>
	<th></th>
</tr>
<c:forEach items="${sessionScope.carrinho}" var="c">
	<tr>
		<td>${c.id}</td>
		<td>${c.descricao}</td>
		<td><small><a href="carrinho.sec?cmd=remove&carrinho=${c.id}" class="action">Remover</a></small></td>
	</tr>
</c:forEach>

</table>
</body>
</html>